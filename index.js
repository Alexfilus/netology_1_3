const ChatApp = require('./chatapp').ChatApp;

let webinarChat = new ChatApp('webinar');
let facebookChat = new ChatApp('=========facebook');
let vkChat = new ChatApp('---------vk');

let chatOnMessage = (message) => {
    console.log(message);
};
let readyToAnswer = () => {
    console.log('Готовлюсь к ответу');
};

webinarChat.on('message', chatOnMessage).on('message', readyToAnswer);
facebookChat.on('message', chatOnMessage);
vkChat.on('message', chatOnMessage).setMaxListeners(2).on('message', readyToAnswer).on('close', chatOnMessage);


// Закрыть вконтакте
setTimeout(() => {
    console.log('Закрываю вконтакте...');
    vkChat.removeAllListeners('message');
    vkChat.close();
}, 3000);


// Закрыть фейсбук
setTimeout(() => {
    console.log('Закрываю фейсбук, все внимание — вебинару!');
    facebookChat.removeListener('message', chatOnMessage);
}, 5000);


setTimeout(() => {
    webinarChat.removeAllListeners('message');
}, 30000);